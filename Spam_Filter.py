import re
import sys
import ConfigParser
import langid
import ret_codes
import logging

class LogConfig :
    def config(self, logfile) :
        logging.basicConfig(filename=logfile, format='%(asctime)s   %(levelname)s : %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)
        return logging

class OutputWriter :
    ''' Write output to a file '''
    def fetch_output_handle (self, output_file) :
        try :
            f_handle = open(output_file, "w")
        except IOError :
            print "Unable to open output file for writing posts."
        return f_handle
    def write_output (self, output_handle, out_str):
        try :
            output_handle.write(out_str)
        except IOError :
            print "Unable to write output to File"

class SpamFilter :
    #_log_obj = logging() 
    '''
    Spam check class.
    Init - initializes the filter from config file.
    '''
    # Initialize all Spam-Filter configuration parameters.
    def __init__ (self, config_file, log_file) :

        new_log_filter = LogConfig()
        SpamFilter.log_obj = new_log_filter.config(log_file)
        config = ConfigParser.RawConfigParser()     # Initialize config parser.

        config_file = 'conf/' + config_file
        if not config.read(config_file) :                                   # Read config file.                    
            print "Unable to read configuration file - ", config_file,". Please check if file exists!!"
            SpamFilter.log_obj.error("Problem reading config file at %s. Quitting..", config_file)
            exit()
        else :
            SpamFilter.log_obj.info("Reading config file at %s", config_file)

        
        # Read flag parameters from config file to set filters to test for.
        if config.has_section('Filter_Flags') :
            if config.has_option('Filter_Flags', 'check_all') :                     # check if check_all flag is present
                SpamFilter.log_obj.info("Initializing config flag [%s] from config file [%s]", 'check_all', config_file)
                self.checkall = config.getboolean('Filter_Flags', 'check_all')      # get value check_all flag
            else : self.checkall = False                                            # if not present, set check_all to default value i.e False
        else :
            print "[Filter_Options] section is missing. Please check configuration file."
            SpamFilter.log_obj.error("Missing section [%s] in Config file. Quitting..",'Filter_Flags')
            exit()
        
        if not config.has_section('Filter_Options') :
            print "[Filter_Options] section is missing. Please check configuration file."
            SpamFilter.log_obj.error("Missing section [%s] in Config file. Quitting..",'Filter_Options')
            exit()

        # overriding all params. Set to TRUE if checkall is set to TRUE
        if self.checkall is True :
            self.check_lang = True
            self.check_email_webaddr_in_title = True
            self.check_title_maxlen = True
            self.check_title_minlen = True
            self.check_phone_in_title = True
            self.check_blacklist_Strings = True
            self.check_blacklist_SocialActions = True
            self.check_blacklist_Symbols = True
            self.check_blacklist_First_Person_Narrative = True
        
        # setting options from config file [Filter_Flags] if checkall is FALSE. DEFAULT is FALSE.
        else :				 
            if config.has_option('Filter_Flags', 'check_title_maxlen') :
                self.check_title_maxlen = config.getboolean('Filter_Flags', 'check_title_maxlen')
            else : self.check_title_maxlen = False

            if config.has_option('Filter_Flags', 'check_title_minlen') :
                self.check_title_minlen = config.getboolean('Filter_Flags', 'check_title_minlen')
            else : self.check_title_minlen = False

            if config.has_option('Filter_Flags', 'check_phone_in_title') :
                self.check_phone_in_title = config.getboolean('Filter_Flags', 'check_phone_in_title')
            else : self.check_phone_in_title = False

            if config.has_option('Filter_Flags', 'check_email_webaddr_in_title') :
                self.check_email_webaddr_in_title = config.getboolean('Filter_Flags', 'check_email_webaddr_in_title')
            else : self.check_email_webaddr_in_title = False

            if config.has_option('Filter_Flags', 'check_lang') :
                self.check_lang = config.getboolean('Filter_Flags', 'check_lang')
            else : self.check_lang = False

            if config.has_option('Filter_Flags', 'check_blacklist_Strings') :
                self.check_blacklist_Strings = config.getboolean('Filter_Flags', 'check_blacklist_Strings')
            else : self.check_blacklist_Strings = False

            if config.has_option('Filter_Flags', 'check_blacklist_SocialActions') :
                self.check_blacklist_SocialActions = config.getboolean('Filter_Flags', 'check_blacklist_SocialActions')
            else : self.check_blacklist_SocialActions = False
            
            if config.has_option('Filter_Flags', 'check_blacklist_Symbols') :
                self.check_blacklist_Symbols = config.getboolean('Filter_Flags', 'check_blacklist_Symbols')
            else : self.check_blacklist_Symbols = False
            
            if config.has_option('Filter_Flags', 'check_blacklist_First_Person_Narrative') :
                self.check_blacklist_First_Person_Narrative = config.getboolean('Filter_Flags', 'check_blacklist_First_Person_Narrative')
            else : self.check_blacklist_First_Person_Narrative = False

        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_lang' , config_file)
        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_email_webaddr_in_title' , config_file)
        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_phone_in_title' , config_file)
        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_title_maxlen' , config_file)
        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_title_minlen' , config_file)
        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_blacklist_Strings' , config_file)
        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_blacklist_SocialActions' , config_file)
        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_blacklist_Symbols' , config_file)
        SpamFilter.log_obj.info("[%s] - Flag initialized from config file [%s]", 'check_blacklist_First_Person_Narrative' , config_file)
        SpamFilter.log_obj.info("All check flags initialized flag from config file [%s]", config_file)            


        # check for filters which are True and Pick respective config parameters from section [Filter_Options].          
        if self.check_blacklist_Strings is True :
            if config.has_option('Filter_Options', 'blacklist_strings_file') :
                self.blacklist_strings_file = config.get('Filter_Options', 'blacklist_strings_file')
                SpamFilter.log_obj.info("Initialized blacklist (Strings) file from config file [%s]", config_file)            
            else :
                print "Blacklist checking option is set to true but there is no Blacklist (Strings) file in [Filter Options] in config file. Please check config file!!"
                SpamFilter.log_obj.error("Blacklist checking option is set to true but there is no Blacklist (Strings) file in [Filter Options] in config file [%s]", config_file)            
                exit()

            self.blacklist_strings = dict()
            self.read_blacklist(self.blacklist_strings_file, self.blacklist_strings)

        if self.check_blacklist_Symbols is True :
            if config.has_option('Filter_Options', 'blacklist_symbols_file') :
                self.blacklist_symbols_file = config.get('Filter_Options', 'blacklist_symbols_file')
                SpamFilter.log_obj.info("Initialized blacklist (Symbols) file from config file [%s]", config_file)            
            else :
                print "Blacklist checking option is set to true but there is no Blacklist (Symbols) file in [Filter Options] in config file. Please check options in config file!!"
                SpamFilter.log_obj.error("Blacklist checking option is set to true but there is no Blacklist (Symbols) file in [Filter Options] in config file [%s]", config_file)            
                exit()
 
            self.blacklist_symbols = dict()
            self.read_blacklist(self.blacklist_symbols_file, self.blacklist_symbols)

        if self.check_blacklist_SocialActions is True :
            if config.has_option('Filter_Options', 'blacklist_socialactions_file') :
                self.blacklist_socialactions_file = config.get('Filter_Options', 'blacklist_socialactions_file')
                SpamFilter.log_obj.info("Initialized blacklist (SocialActions) file from config file [%s]", config_file)            
            else :
                print "Blacklist checking option is set to true but there is no Blacklist (SocialActions) file in [Filter Options] in config file.  Please check options in config file!!"
                SpamFilter.log_obj.error("Blacklist checking option is set to true but there is no Blacklist (SocialActions) file in [Filter Options] in config file [%s]", config_file)            
                exit()

            self.blacklist_socialactions = dict()
            self.read_blacklist(self.blacklist_socialactions_file, self.blacklist_socialactions)

        if self.check_blacklist_First_Person_Narrative is True :
            if config.has_option('Filter_Options', 'blacklist_first_person_narrative_file') :
                self.blacklist_first_person_narrative_file = config.get('Filter_Options', 'blacklist_first_person_narrative_file')
                SpamFilter.log_obj.info("Initialized blacklist (FirstPersonNarrative) file from config file [%s]", config_file)            
            else :
                print "Blacklist checking option is set to true but there is no Blacklist First_Person_Narrative file in [Filter Options] in config file.  Please check options in config file!!"
                SpamFilter.log_obj.error("No Blacklist (FirstPersonNarrative) file in [Filter Options] in config file [%s]", config_file)            
                exit()

            self.blacklist_firstpersonnarrative = dict()
            self.read_blacklist(self.blacklist_first_person_narrative_file, self.blacklist_firstpersonnarrative)

        if (self.check_email_webaddr_in_title) :
            if (config.has_option('Filter_Options', 'reg_email')):
                self.reg_email = config.get('Filter_Options', 'reg_email')		# fetch regex to match email
                SpamFilter.log_obj.info("Initialized EMail regex from config file [%s]", config_file)            
            else : 
                SpamFilter.log_obj.error("EMail regex missing from config file [%s]", config_file)            
                print "EMail check set to true but missing regex for E-Mail match in [Filter Options] in rules config file. Please check the option"
                exit()

            if (config.has_option('Filter_Options', 'reg_web_addr')):
                self.reg_web_addr = config.get('Filter_Options', 'reg_web_addr')	# fetch regex to match web address
                SpamFilter.log_obj.info("Initialized Web Addr regex from config file [%s]", config_file)            
            else : 
                SpamFilter.log_obj.error("Web Addr regex missing from config file [%s]", config_file)            
                print "Web Address check set to true but missing regex for WebAddr match in [Filter Options] in rules config file. Please check the option"
                exit()

        if (self.check_phone_in_title) :
            if (config.has_option('Filter_Options', 'reg_phone')) :
                self.reg_phone = config.get('Filter_Options', 'reg_phone')          # fetch regex to match phone number
                SpamFilter.log_obj.info("Initialized Phone Number regex from config file [%s]", config_file)            
            else :
                SpamFilter.log_obj.error("Phone Number regex missing from config file [%s]", config_file)            
                print "Phone Number check in title set to true but missing regex for Phone number match in [Filter Options] in rules config file. Please check the option"
                exit()
            
        if (self.check_title_maxlen) :
            if config.has_option('Filter_Options', 'title_maxlen') :						
                self.title_maxlen = config.getint('Filter_Options', 'title_maxlen') # fetch max title length allowed
                SpamFilter.log_obj.info("Initialized Max Title Length from config file [%s]", config_file)            
            else :
                SpamFilter.log_obj.error("Max Title Length missing from config file [%s]", config_file)            
                print "Title length check set to true but missing max allowed length for title in rules config file. Please check the option"
                exit()
                
        if (self.check_title_minlen) :
            if config.has_option('Filter_Options', 'title_minlen') :                        
                self.title_minlen = config.getint('Filter_Options', 'title_minlen') # fetch max title length allowed
                SpamFilter.log_obj.info("Initialized Min Title Length from config file [%s]", config_file)            
            else :
                SpamFilter.log_obj.error("Min Title Length missing from config file [%s]", config_file)            
                print "Title length check set to true but missing min allowed length for title in rules config file.  Please check the option"
                exit()

        if (self.check_lang) :
            if config.has_option('Filter_Options', 'language') :                        
                self.language = config.get('Filter_Options', 'language') # fetch language allowed
                SpamFilter.log_obj.info("Initialized default Language to check from config file [%s]", config_file)            
            else :
                SpamFilter.log_obj.error("Default Language allowed is missing from config file [%s]", config_file)            
                print "Language check set to true but missing language that is allowed for title in rules config file. Please check the option"
                exit()

            if config.has_option('Filter_Options', 'lang_min_confidence') :                        
                self.lang_min_confidence = config.getfloat('Filter_Options', 'lang_min_confidence') # fetch minimum confidence for the language
                SpamFilter.log_obj.info("Initialized min Confidence required for language from config file [%s]", config_file)            
            else :
                SpamFilter.log_obj.error("Minimum Confidence required is missing from config file [%s]", config_file)            
                print "Language check set to true but missing minimum confidence allowed for any language in rules config file. Please check the option"
                exit()
        SpamFilter.log_obj.info("*********************************\nInitialized config file - [%s]\n*********************************", config_file)            


    def read_blacklist(self,black_list_file, black_list_dict) :            # fill blacklist dictionary.
        try :
            SpamFilter.log_obj.info("Initializing blacklist file [%s]",black_list_file)            
            black_list_file  = 'conf/' + black_list_file
            with open(black_list_file, "r") as f :
                for line in f:
                    line = line.strip()
                    words = line.split(' ')
                    i = 0
                    words_num = len(words)
                    word = ''
                    for i in range(0, words_num - 1) :
                        if word is not '' :
                            word = word +' '+ words[i]
                        else :
                            word = word + words[i]
                        if word in black_list_dict and black_list_dict[word] is not 1 :
                            black_list_dict[word] = 0
                    if word is not '' :
                        word = word + ' ' + words[words_num -1]
                    else :
                        word = word + words[words_num -1]
                    black_list_dict[word] = 1
        except IOError :
            SpamFilter._log_obj.error("Error in reading blacklist file [%s].",black_list_file)            
            print "Can't open blacklist file, i.e" , self.blacklist_strings_file, " for reading. Please check that blacklist file has read permissions."

    def title_length_maxlen (self, text):                   # check if text exceeds max_len
        if (len(text) > self.title_maxlen) :
            print 'Rejected! Title length > ' , self.title_maxlen , ' chars.'
            return ret_codes.err_Codes.TILE_LEN_GREATER
        else :
            print 'Accepted! Title length < ' , self.title_maxlen , ' chars.'
            return ret_codes.err_Codes.SUCCESS

    def title_length_minlen (self, text):	              # check if text is less than min_len
        if (len(text) < self.title_minlen) :
            print 'Rejected! Title length ',len(text),' < ' , self.title_minlen , ' chars.'
            return ret_codes.err_Codes.TITLE_LEN_LESSER
        else :
            print 'Accepted! Title length > ' , self.title_minlen , ' chars.'
            return ret_codes.err_Codes.SUCCESS

    def title_blacklist_Strings (self, text) :          # check for Blacklist words - Strings Blacklist file
        words = text.split ( ' ' )
        i = 0;
        
        word_list_len = len(words)
        while i < word_list_len :
            if words[i] in self.blacklist_strings :
                word = words[i]

                while word in self.blacklist_strings and self.blacklist_strings[word] == 0 and i < word_list_len -1 :
                    i = i + 1
                    word_new = words[i]
                     
                    word = word +' '+ word_new
                if word in self.blacklist_strings and self.blacklist_strings[word] == 1 :
                    print 'Rejected! Word ', word ,' exists in the blacklist.'
                    return ret_codes.err_Codes.BLACKLIST_WORD_ERR

            else : i = i + 1
        print 'Accepted! Every Item passed on Blacklist Strings filter.'
        return ret_codes.err_Codes.SUCCESS
        
    def title_blacklist_SocialActions (self, text) :          # check for Blacklist words - SocialActions Blacklist file
        words = text.split ( ' ' )
        i = 0;
        j = 0;
        word_list_len = len(words)
        while i < word_list_len :
            if words[i] in self.blacklist_socialactions :
                word = words[i]

                while word in self.blacklist_socialactions and self.blacklist_socialactions[word] == 0 and i < word_list_len -1 :
                    i = i + 1
                    word_new = words[i]
                     
                    word = word +' '+ word_new
                if word in self.blacklist_socialactions and self.blacklist_socialactions[word] == 1 :
                    j = j + 1
                    i = i + 1

                if j == 2 :
                    print 'Rejected! 2 Words exist in the blacklist social actions file.'
                    return ret_codes.err_Codes.BLACKLIST_WORD_ERR

            else : i = i + 1
        print 'Accepted! Every Item passed on Blacklist Social Actions filter.'
        return ret_codes.err_Codes.SUCCESS

    def title_blacklist_Symbols (self, text) :          # check for Blacklist words - Symbols Blacklist file
        words = text.split ( ' ' )
        i = 0;
        
        word_list_len = len(words)
        while i < word_list_len :
            if words[i] in self.blacklist_symbols :
                word = words[i]

                while word in self.blacklist_symbols and self.blacklist_symbols[word] == 0 and i < word_list_len -1 :
                    i = i + 1
                    word_new = words[i]
                     
                    word = word +' '+ word_new
                if word in self.blacklist_symbols and self.blacklist_symbols[word] == 1 :
                    print 'Rejected! Word ', word ,' exists in the blacklist symbols file.'
                    return ret_codes.err_Codes.BLACKLIST_WORD_ERR

            else : i = i + 1
        print 'Accepted! Every Item passed on Blacklist Symbols filter.'
        return ret_codes.err_Codes.SUCCESS

    def title_blacklist_First_Person_Narrative (self, text) :          # check for Blacklist words - First Person Narrative Blacklist file
        words = text.split ( ' ' )
        i = 0
        j = 0
        word_list_len = len(words)
        while i < word_list_len :
            if words[i] in self.blacklist_firstpersonnarrative :
                word = words[i]

                while word in self.blacklist_firstpersonnarrative and self.blacklist_firstpersonnarrative[word] == 0 and i < word_list_len -1 :
                    i = i + 1
                    word_new = words[i]
                     
                    word = word +' '+ word_new

                if word in self.blacklist_firstpersonnarrative and self.blacklist_firstpersonnarrative[word] == 1 :
                    j = j + 1
                    i = i + 1

                if j == 3 :
                    print 'Rejected! 3 Words exist in the blacklist first person narrative file.'
                    return ret_codes.err_Codes.BLACKLIST_WORD_ERR

            else : i = i + 1
        print 'Accepted! Every Item passed on Blacklist First Persons Narrative filter.'
        return ret_codes.err_Codes.SUCCESS
       
    def check_phone_num (self, text) :		# check if text has a phone number
        '''
        regular expression for phone numbers :-
        xxx-xxx-xxxx, xxx xxx xxxx, xxx.xxx.xxxx, (xxx)xxx-xxxx, (xxx)xxx xxxx, (xxx)xxx.xxxx, (xxx) xxx-xxxx,
        (xxx) xxx xxxx, (xxx) xxx.xxxx, xxx-xxxx, xxx xxxx, xxx.xxxx, xxxxxxx, xxxxxxxxxx, (xxx)xxxxxxx
        '''
        results = re.match( self.reg_phone, text)

        if (results) :
            print 'Rejected! as title has a phone number.'
            return ret_codes.err_Codes.PHONE_IN_TITLE_ERR
        else :
            print 'Accepted! as title is not having any phone number.'
            return ret_codes.err_Codes.SUCCESS      
        
    def check_lang_post (self, text):       # check if text is in English.
        lang_var = langid.classify(text)
        if lang_var[0] != 'en' :
            print "Rejected! Language detected is ",lang_var[0], " and we just have to filter English text."
            return ret_codes.err_Codes.LANG_ERR
        if lang_var[1] < self.lang_min_confidence :
            print "Rejected! Language detected is ",lang_var[0], " but it's confidence is ", lang_var[1], " which is less than min_conf ", self.lang_min_confidence,"."                              # test
            return ret_codes.err_Codes.LANG_ERR
        else :
            print "Accepted! Language detected is ",lang_var[0], " and it's confidence is ", lang_var[1], " which is more than min_conf - ", self.lang_min_confidence," required."                              # test
            return ret_codes.err_Codes.SUCCESS      
        
    def check_web_email(self, text) :       # check if text has just 1 word and that is E-Mail or Web-Address
        i = 0
        for word in text :
            if word == ' ' :
                i = i +1 
        words_num = i+1

        if words_num > 1 :
            print 'Accepted! No Email and Website check required as text has more than 1 word.'
            return ret_codes.err_Codes.SUCCESS
        # results_web = re.match( self.reg_web_addr, text)
        else :
            email_regex = re.compile(self.reg_email)
            if (email_regex.match(text)) :
                print 'Rejected! as title has just an E-Mail Id.'
                return ret_codes.err_Codes.EMAIL_IN_TITLE_ERR
            else :
                print "Accepted! as there is no EMail id in the title"

            web_regex = re.compile(self.reg_web_addr)
            if web_regex.match(text):
                print 'Rejected! as title has just a web address.'
                return ret_codes.err_Codes.WEBADDR_IN_TITLE
            else :
                print 'Accepted! as there is no web address in title'
                return ret_codes.err_Codes.SUCCESS
        
    def check_rules(self, post):							# check rules.
        ret_code = 0
        if (self.check_email_webaddr_in_title) :
            SpamFilter.log_obj.info("CHECKING post[%s] for EMail or WebAddr in title...",str(post['id']))            
            ret_code = self.check_web_email(post['title'])
        if ret_code == ret_codes.err_Codes.EMAIL_IN_TITLE_ERR :
            SpamFilter.log_obj.error("FAILED post[%s] for EMail in title...",str(post['id']))            
            return [ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.EMAIL_IN_TITLE_ERR]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for EMail in title...",str(post['id']))            
            
        if ret_code == ret_codes.err_Codes.WEBADDR_IN_TITLE :
            SpamFilter.log_obj.info("FAILED post[%s] for WebAddr in title...",str(post['id']))            
            return [ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.WEBADDR_IN_TITLE]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for WebAddr in title...",str(post['id']))            

        if (self.check_phone_in_title) :
            SpamFilter.log_obj.info("CHECKING post[%s] for Phone in title...",str(post['id']))            
            ret_code = self.check_phone_num (post['title'])
        if ret_code == ret_codes.err_Codes.PHONE_IN_TITLE_ERR :
            SpamFilter.log_obj.info("FAILED post[%s] for Phone Number in title...",str(post['id']))            
            return [ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.PHONE_IN_TITLE_ERR]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for Phone Number in title...",str(post['id']))            

        if (self.check_title_maxlen) :
            SpamFilter.log_obj.info("CHECKING post[%s] for maximum title length...",str(post['id']))            
            ret_code = self.title_length_maxlen ( post['title'])
        if ret_code == ret_codes.err_Codes.TILE_LEN_GREATER :
            SpamFilter.log_obj.info("FAILED post[%s] for maximum title length...",str(post['id']))            
            return [ ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.TILE_LEN_GREATER ]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for maximum title length...",str(post['id']))            

        if (self.check_title_minlen) :
            SpamFilter.log_obj.info("CHECKING post[%s] for minimum title length...",str(post['id']))            
            ret_code = self.title_length_minlen ( post['title'])
        if ret_code == ret_codes.err_Codes.TITLE_LEN_LESSER :
            SpamFilter.log_obj.info("FAILED post[%s] for minimum title length...",str(post['id']))            
            return [ ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.TITLE_LEN_LESSER ]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for minimum title length...",str(post['id']))            

        if (self.check_blacklist_Strings) :
            SpamFilter.log_obj.info("CHECKING post[%s] for blacklist (Strings)...",str(post['id']))            
            ret_code = self.title_blacklist_Strings (post['title'])
        if ret_code == ret_codes.err_Codes.BLACKLIST_WORD_ERR :
            SpamFilter.log_obj.info("FAILED post[%s] for blacklist (Strings)...",str(post['id']))            
            return [ ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.BLACKLIST_WORD_ERR ]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for blacklist (Strings)...",str(post['id']))            

        if (self.check_blacklist_SocialActions) :
            SpamFilter.log_obj.info("CHECKING post[%s] for blacklist (SocialActions)...",str(post['id']))            
            ret_code = self.title_blacklist_SocialActions (post['title'])
        if ret_code == ret_codes.err_Codes.BLACKLIST_WORD_ERR :
            SpamFilter.log_obj.info("FAILED post[%s] for blacklist (SocialActions)...",str(post['id']))            
            return [ ret_codes.err_Codes.FAILURE,  ret_codes.err_Codes.BLACKLIST_WORD_ERR ]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for blacklist (SocialActions)...",str(post['id']))            

        if (self.check_blacklist_Symbols) :
            SpamFilter.log_obj.info("CHECKING post[%s] for blacklist (Symbols)...",str(post['id']))            
            ret_code = self.title_blacklist_Symbols (post['title'])
        if ret_code == ret_codes.err_Codes.BLACKLIST_WORD_ERR :
            SpamFilter.log_obj.info("FAILED post[%s] for blacklist (Symbols)...",str(post['id']))            
            return [ ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.BLACKLIST_WORD_ERR ]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for blacklist (Symbols)...",str(post['id']))            

        if (self.check_blacklist_First_Person_Narrative) :
            SpamFilter.log_obj.info("CHECKING post[%s] for blacklist (First Person Narrative)...",str(post['id']))            
            ret_code = self.title_blacklist_First_Person_Narrative (post['title'])
        if ret_code == ret_codes.err_Codes.BLACKLIST_WORD_ERR :
            SpamFilter.log_obj.info("FAILED post[%s] for blacklist (First Person Narrative)...",str(post['id']))            
            return [ ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.BLACKLIST_WORD_ERR]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for blacklist (First Person Narrative)...",str(post['id']))            

        if (self.check_lang) :
            SpamFilter.log_obj.info("CHECKING post[%s] for language filter...",str(post['id']))            
            ret_code = self.check_lang_post ( post['title'])
        if ret_code == ret_codes.err_Codes.LANG_ERR :
            SpamFilter.log_obj.info("FAILED post[%s] for language filter...",str(post['id']))            
            return [ret_codes.err_Codes.FAILURE, ret_codes.err_Codes.LANG_ERR]
        else :
            SpamFilter.log_obj.info("PASSED post[%s] for language filter...",str(post['id']))            

        
        SpamFilter.log_obj.info("PASSED post[%s] for on all filters. SUCCESS...",str(post['id']))            
        return [ ret_codes.err_Codes.SUCCESS, ret_codes.err_Codes.SUCCESS]

if __name__ == '__main__' :
    print "Don't run this file directly."
    '''
    spam_filter = SpamFilter ( config_file = 'rules.cfg')                # initialize rule engine with config file
    # Get posts from Linkedin or any file 
    spam_filter.check_Spam_Detection_Rules(post_list, output_file = 'LinkedinOutput1.txt') # check rules on the post_list retrieved from any source.
    '''