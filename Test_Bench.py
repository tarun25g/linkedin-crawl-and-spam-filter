from Spam_Filter import SpamFilter
from Spam_Filter import OutputWriter
from Linkedin_Input import Linkedin_Read
from File_Input import File_Read 
import linkedin_tokens

if __name__ == '__main__' :
    output_file = 'OutFile.txt' 
    spam_filter = SpamFilter ( config_file = 'rules.cfg', log_file = 'SpamFilter_Logs.log')                # initialize rule engine with config file

    choice = raw_input('Enter your input :- Direct_Linkedin(L) or File(F)')
    ##################################################
    if choice == 'L' :
        # Get posts from Linkedin
        linkedin_read = Linkedin_Read()         # initiate a Linkedin Crawler
        
        # passing parameters to Linkedin_Crawler whiFch returns a post_list after Crawling Data
        post_list = linkedin_read.get_post(consumer_key = linkedin_tokens.LINKEDIN_CONSUMER_KEY,
                                            consumer_secret = linkedin_tokens.LINKEDIN_CONSUMER_SECRET,
                                            user_token = linkedin_tokens.LINKEDIN_USER_TOKEN,
                                            user_secret = linkedin_tokens.LINKEDIN_USER_SECRET,
                                            scrap_group_id = linkedin_tokens.linkedin_scrap_group_id, 
                                            return_url = linkedin_tokens.LINKEDIN_RETURN_URL,
                                            )
        
    elif choice == 'F':
        file_read = File_Read()     
        post_list = file_read.get_post('data.txt')          # Read json Data from file "Data.txt". Ret value is post_list
######################################################################

    op_writer = OutputWriter()              # initiating an Output writer
    f_handle = op_writer.fetch_output_handle(output_file)


    for post in post_list :         # Run spam filter for entire post list.
        ret_code = spam_filter.check_rules(post)        # ret_cFode [Status, Reason]

        post_title = post['title'].encode('utf-8')
        summary = str(post['attachment'].get('summary','N/A')).strip() if post.get('attachment') else 'N/A'
        
        out_str = str(post['id'])+"^"+str(ret_code[0])+"^"+str(post['category']['code'])+"^"+post_title+"^"+summary+"\n"

        op_writer.write_output(f_handle, out_str)    # Writing output in desired Format string - out_str in output file
