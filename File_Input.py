import json

class File_Read :
    def get_post (self, input_file) :
        with open(input_file) as data_file :    
            post_list = json.load(data_file)
        return post_list
