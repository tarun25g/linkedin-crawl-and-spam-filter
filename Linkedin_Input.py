from linkedin_crawl import Linkedin_Crawl

class Linkedin_Read :
    def get_post(self, consumer_key, consumer_secret, user_token, user_secret, scrap_group_id, return_url) :

        linkedin_crawl = Linkedin_Crawl()
        post_list = linkedin_crawl.fetch_Linkedin (CONSUMER_KEY = consumer_key,
                                                    CONSUMER_SECRET = consumer_secret,
                                                    USER_TOKEN = user_token,
                                                    USER_SECRET = user_secret,
                                                    scrap_group_id = scrap_group_id, 
                                                    RETURN_URL = return_url)
        return post_list

