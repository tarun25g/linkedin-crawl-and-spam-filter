#from __future__ import print_function
from linkedin import linkedin
import json
import time

class Linkedin_Crawl :
    def fetch_Linkedin(self, CONSUMER_KEY, CONSUMER_SECRET, USER_TOKEN, USER_SECRET,scrap_group_id, RETURN_URL ) :
        posts_file='posts_' + str(scrap_group_id) + '.txt'
        posts_json_fname='posts_' + str(scrap_group_id) + '.json'
        #posts_file = open(posts_file,'w')
        posts_json_file= open ( posts_json_fname,'w' )
        
        authentication = linkedin.LinkedInDeveloperAuthentication ( CONSUMER_KEY, CONSUMER_SECRET, 
                                                                  USER_TOKEN, USER_SECRET, 
                                                                  RETURN_URL, linkedin.PERMISSIONS.enums.values() )
        application = linkedin.LinkedInApplication(authentication)
        
        param_count=2000
        param_start=0
        loop=0
        
        #write posts in character delimited file
        # posts_header = 'parse_post_id' + '^' + 'parse_post_type' + '^' + 'parse_post_category' + '^' + 'parse_post_title' + '^' + 'parse_creator_id' + '^'  +\
        #                 'parse_creator_distance' + '^' + 'parse_likes_total' + '^'  + 'parse_comments_total' + '^' + 'parse_attachment_domain' + '^' + 'parse_attachment_url'
        # print (posts_header, file=posts_file)
        
        while loop<6:
            posts_json= application.get_posts(scrap_group_id, selectors= [ 'id', 'type', 'category', 'title', 
                                                                {'creator':['id','firstName', 'lastName','location','headline','industry',
                                                                            'distance']},
                                                                'summary', 'likes','creation-timestamp',
                                                                {'comments':['id','text','creation-timestamp','relation-to-viewer:(available-actions)',
                                                                            {'creator':['id','firstName', 'lastName','location','headline','industry',
                                                                            'distance']}]},
                                                                 'relation-to-viewer:(is-following)',
                                                                 'site-group-post-url','attachment'] ,
                                                     params={'count': param_count, 'order' : 'recency', 'start' : param_start})
        
            posts_cnt= posts_json.get('_count', posts_json.get('_total',0))
        
            first_post_id= posts_json['values'][0]['id']
            last_post_id=posts_json['values'][posts_cnt-1]['id']
        #    loop = loop + 1
        
            post_list = list()
            # write posts in character delimited file
            for post in posts_json['values'] :
               parse_post_title = "'" + str(post['title'].encode('utf-8')).strip() + "'";
               #print parse_post_title
        #       parse_post_text = "'" + str(post['text'].encode('utf-8')).strip() + "'"
               parse_post_id = str(post['id']) 
               parse_post_type =str(post['type'])
               parse_post_category = str(post['category'])
               parse_creator_id = str(post['creator'].get('id','N/A')) if post.get('creator') else 'N/A'
               parse_creator_distance = str(post['creator'].get('distance','N/A')) if post.get('creator') else 'N/A'
               parse_likes_total = str(post['likes'].get('_total','0')) if post.get('likes') else 'N/A'
               parse_comments_total = str(post['comments'].get('_total','0')) if post.get('comments') else 'N/A'
               parse_attachment_domain = str(post['attachment'].get('content-domain','N/A')) if post.get('attachment') else 'N/A'
               parse_attachment_url = str(post['attachment'].get('content-url','N/A')).strip() if post.get('attachment') else 'N/A'
        
               post_record=parse_post_id + '^' + parse_post_type + '^' + parse_post_category + '^' + parse_post_title + '^' + parse_creator_id + '^'  +\
                            parse_creator_distance+ '^' + parse_likes_total + '^'  + parse_comments_total + '^' + parse_attachment_domain + '^' + parse_attachment_url
               #print post_record;
               post_list.append(post)
               #break;
            param_start=param_start+posts_cnt-1
            loop += 1
            return post_list
        #        print( post_record, file = posts_file )
        
        #        print json for posts in file
        #        print (json.dumps(posts_json,sort_keys=True, indent=4), file= posts_json_file)
            

        
        
        
        #posts_json_file.close()


if __name__ == '__main__' :
    #populate this using your credentials
    LINKEDIN_CONSUMER_KEY = '3iema67ht5qv'
    LINKEDIN_CONSUMER_SECRET = '5s6Zc3FwXDTsW8zA'

    LINKEDIN_USER_TOKEN = 'b7e5f63e-8acb-4eb6-8c62-a9570089cfce'
    LINKEDIN_USER_SECRET = 'd70b7a7c-edf2-485b-837b-eac18903a3ef'

    #Group_ID to scrap
    linkedin_scrap_group_id = 125258
    LINKEDIN_RETURN_URL='http://localhost:8080/'


    linkedin_crawl = Linkedin_Crawl()
    post_list = linkedin_crawl.fetch_Linkedin (CONSUMER_KEY = LINKEDIN_CONSUMER_KEY,
                                  CONSUMER_SECRET = LINKEDIN_CONSUMER_SECRET,
                                  USER_TOKEN = LINKEDIN_USER_TOKEN,
                                  USER_SECRET = LINKEDIN_USER_SECRET,
                                  scrap_group_id = linkedin_scrap_group_id, 
                                  RETURN_URL = LINKEDIN_RETURN_URL)
    for post in post_list :
        print post
